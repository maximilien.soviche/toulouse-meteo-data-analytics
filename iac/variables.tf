variable "project" {
  type = string
}

variable "region" {
  type    = string
  default = "europe-west1"
}

variable "module_name" {
  type    = string
  default = "toulouse-meteo-connector"
}

variable "module_revision" {
  type = string
}
