resource "google_cloud_run_service" "default" {
  name     = var.module_name
  location = var.region


  template {
    spec {
      service_account_name = google_service_account.service_account.email
      containers {
        image = "gcr.io/${var.project}/${var.module_name}@${var.module_revision}"

        env {
          name = "BACKFILL_BUCKET"
          value = google_storage_bucket.backfill_bucket.name
        }
      }
    }

    metadata {
      annotations = {
        "autoscaling.knative.dev/maxScale" = "1"
      }
    }
  }

  traffic {
    percent         = 100
    latest_revision = true
  }

  depends_on = [google_project_service.apis]
}
