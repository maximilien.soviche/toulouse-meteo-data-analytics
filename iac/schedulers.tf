resource "google_cloud_scheduler_job" "trigger_extract" {
  project = var.project
  name             = "sch-trigger-${var.module_name}"
  description      = "Trigger main extract connector"
  schedule         = "*/15 * * * *"
  time_zone        = "Europe/Paris"
  attempt_deadline = "320s"

  http_target {
    http_method = "POST"
    uri         = "${google_cloud_run_service.default.status.0.url}/v1/all/recent"

    oidc_token {
      service_account_email = google_service_account.service_account.email
    }
  }

  depends_on = [google_project_service.apis]
}
